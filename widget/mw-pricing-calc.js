(function(window, undefined) {
  var MWCalc = {};

  MWCalc.init = function() {
    var self = this;
    console.log("HELLO");
    self.loadScript('//widget.dev/jquery-2.1.1.min.js', function(){
      console.log("loaded");
      self.$ = self.jQuery = jQuery.noConflict(true);
      self.render();
    });
  };
  MWCalc.render = function() {
    var self = this;
    console.log("render");
    self.$('#mw-calc').after("<form id='mwc-form'><input type='text' name='name'></input></form>");
    self.$("#mwc-form").on("change", "input", function(){
      self.handleChange();
    });

  };
  MWCalc.handleChange = function(){
    var self = this;
    self.$('#mwc-form').after("<div>CHANGED</div>");
  };
  MWCalc.loadScript = function(url, callback) {
    var script = document.createElement('script');
    script.async = true;
    script.src = url;

    var entry = document.getElementsByTagName('script')[0];
    entry.parentNode.insertBefore(script, entry);

    script.onload = script.onreadystatechange = function() {
      var rdyState = script.readyState;

      if (!rdyState || /complete|loaded/.test(script.readyState)) {
        callback();
        script.onload = null;
        script.onreadystatechange = null;
      }
    };
  };

  window.MWCalc = MWCalc;
})(this);

MWCalc.init();
